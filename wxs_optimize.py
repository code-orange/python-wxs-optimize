import json
import os
import uuid
from xml.sax.saxutils import quoteattr as xml_quoteattr

import xmltodict


def dir_as_xml(path):
    try:
        result = (
            "<Directory Id="
            + xml_quoteattr("id_" + project_files[path]["wxs_id"].replace("-", "_"))
            + " Name="
            + xml_quoteattr(os.path.basename(path))
            + ">\n"
        )
    except KeyError:
        result = "<Directory Name=" + xml_quoteattr(os.path.basename(path)) + ">\n"

    for item in os.listdir(path):
        itempath = os.path.join(path, item)
        if os.path.isdir(itempath):
            result += "\n".join(
                "  " + line for line in dir_as_xml(os.path.join(path, item)).split("\n")
            )
        elif os.path.isfile(itempath):
            if item not in filename_blacklist:
                wxs_id_list.append(project_files[itempath]["wxs_id"])

                result += (
                    "  <Component Id="
                    + xml_quoteattr(
                        "id_" + project_files[itempath]["wxs_id"].replace("-", "_")
                    )
                    + " DiskId="
                    + xml_quoteattr("1")
                    + " Guid="
                    + xml_quoteattr(project_files[itempath]["wxs_id"])
                    + "><File Id="
                    + xml_quoteattr(
                        "id_" + project_files[itempath]["wxs_id"].replace("-", "_")
                    )
                    + " Name="
                    + xml_quoteattr(item)
                    + ' Source="'
                    + itempath.replace("/", "\\")
                    + '"></File></Component>\n'
                )
    result += "</Directory>\n"
    return result


project_files = json.load(open("files.json", "r"))
project_installer = xmltodict.parse(open("installer.wxs", "r").read())

subdir = "files"

wxs_id_list = list()

filename_blacklist = (
    ".git",
    ".gitmodule",
    ".gitignore",
    ".gitattributes",
)

for root, dirs, files in os.walk(subdir):
    for dir in dirs:
        dirname = os.path.join(root, dir)

        if dirname not in project_files:
            project_files[dirname] = dict()

        if "wxs_id" not in project_files[dirname]:
            project_files[dirname]["wxs_id"] = str(uuid.uuid4())

    for file in files:
        filename = os.path.join(root, file)

        if filename not in project_files:
            project_files[filename] = dict()

        if "wxs_id" not in project_files[filename]:
            project_files[filename]["wxs_id"] = str(uuid.uuid4())

project_installer["Wix"]["Product"]["Directory"]["Directory"]["Directory"]["Directory"][
    "Directory"
] = xmltodict.parse(dir_as_xml(subdir))["Directory"]["Directory"]
project_installer["Wix"]["Product"]["Directory"]["Directory"]["Directory"]["Directory"][
    "Component"
] = xmltodict.parse(dir_as_xml(subdir))["Directory"]["Component"]

project_installer["Wix"]["Product"]["Feature"]["ComponentRef"] = list()

wxs_id_list = list(dict.fromkeys(wxs_id_list))

for wxs_id in wxs_id_list:
    project_installer["Wix"]["Product"]["Feature"]["ComponentRef"].append(
        {
            "@Id": "id_" + wxs_id.replace("-", "_"),
        }
    )

with open("files.json", "w") as files_json:
    json.dump(project_files, files_json, indent=4)

with open("installer.wxs", "w") as installer_wxs:
    installer_wxs.write(
        xmltodict.unparse(project_installer, pretty=True, indent="    ")
    )
